This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Jackson Relocator


## [v1.0.0] - 2017-04-29

- First Release

