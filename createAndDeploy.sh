#!/bin/bash

jacksonGroupId=com.fasterxml.jackson.core
jacksonVersion=2.8.11

jacksonOriginalPackage=com.fasterxml.jackson
gCubePrefix=org.gcube

gCubeGroupId=org.gcube.common

repositoryId=gcube-releases
repositoryUrl=http://maven.research-infrastructures.eu/nexus/content/repositories/gcube-releases

username=
password=

function showhelp {
	echo -e "\nUsage: createAndDeploy.sh -u <nexus-releases-username> -p <nexus-releases-password> [-h]\n"
	echo -e " -h  = shows this help.\n"
}

while getopts ":u:p:h" opt; do
	case $opt in
		u) username=$OPTARG;;
		p) password=$OPTARG;;
    	h) showhelp
			exit 0 ;;
		\?) echo -e "\nERROR:invalid option: -$OPTARG"; 
			showhelp; 
			echo -e "\naborting.\n" 
			exit 1 >&2 ;;
  	esac
done

settingsFile=jenkins-releases-settings.xml
rm ${settingsFile}
wget https://code-repo.d4science.org/gCubeSystem/Configs/raw/branch/master/Maven/1.1.0/${settingsFile}

sed -i 's/{{ gcube_maven_user }}/'"${username}"'/g' ${settingsFile}
sed -i 's/{{ gcube_maven_pwd }}/'"${password}"'/g' ${settingsFile}

for i in jackson-core jackson-annotations jackson-databind
do
	jacksonArtifactId=$i

	cp pom.repackage.template.xml pom.xml
	
	sed -i 's/JACKSON_GROUP_ID/'"${jacksonGroupId}"'/g' pom.xml
	sed -i 's/JACKSON_ARTIFACT_ID/'"${jacksonArtifactId}"'/g' pom.xml
	sed -i 's/JACKSON_VERSION/'"${jacksonVersion}"'/g' pom.xml
	
	sed -i 's/JACKSON_ORIGINAL_PACKAGE/'"${jacksonOriginalPackage}"'/g' pom.xml
	sed -i 's/GCUBE_PREFIX/'"${gCubePrefix}"'/g' pom.xml
	
	projectDir=${PWD}
	
	mvn -U clean package
	cd target
	
	repackagedJacksonArtifact=repackaged-${jacksonArtifactId}-${jacksonVersion}
	unzip ${repackagedJacksonArtifact}.jar -d ${repackagedJacksonArtifact}
	
		
	if [ -d "${repackagedJacksonArtifact}/META-INF/services" ]; then
  		cd ${repackagedJacksonArtifact}/META-INF/services
  		for FILE in ${jacksonOriginalPackage}.*
		do
		    sed -i 's/'"${jacksonOriginalPackage}"'/'"${gCubePrefix}.${jacksonOriginalPackage}"'/g' ${FILE}
		    mv ${FILE} ${gCubePrefix}.${FILE}
		done
	fi
	
	gCubeArtifactJar=gcube-${jacksonArtifactId}-${jacksonVersion}.jar
	gCubeArtifactId=gcube-${jacksonArtifactId}
	
	cd ${projectDir}/target/${repackagedJacksonArtifact}
	zip -r ../${gCubeArtifactJar} .
	
	cd ${projectDir}
	mkdir ${gCubeArtifactId}
	mv 	${projectDir}/target/${gCubeArtifactJar} ${gCubeArtifactId}
	
	mvn clean
	rm pom.xml
	rm dependency-reduced-pom.xml
	
	cd ${gCubeArtifactId}
	
	cp ${projectDir}/pom.deploy.template.${jacksonArtifactId}.xml pom.xml
	sed -i 's/JACKSON_ARTIFACT_ID/'"${jacksonArtifactId}"'/g' pom.xml
	sed -i 's/JACKSON_VERSION/'"${jacksonVersion}"'/g' pom.xml
	
	mvn -s ../jenkins-releases-settings.xml -U deploy:deploy-file -DpomFile=pom.xml -Dfile=${gCubeArtifactJar} -DrepositoryId=${repositoryId} -Durl=${repositoryUrl}
		
	cd ${projectDir}
	rm -rf ${gCubeArtifactId}
done

cd ${projectDir}
rm ${settingsFile}
